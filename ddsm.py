import os
import cv2
import csv
import numpy as np
from tqdm import tqdm
import skimage.filters
import skimage.morphology

def floor(image):
    nbins = 1000
    hist = np.histogram(image, bins=nbins)
    #print(hist[0][:300])

    thresh = np.prod(image.shape)//4

    total = 0
    for pop, edge in zip(*hist):
        total += pop
        if total > thresh:
            #print('floor:', edge, 'min:', np.min(image))
            return edge

    assert(False)
    return 0

def gen_mask(image, thresh=0.1):
    image = skimage.filters.gaussian(image,4)

    #thresh = #skimage.filters.threshold_li(image)
    #print("Thresholding at ",thresh)
    mask = image > thresh

    area = np.prod(image.shape)
    bigstuff = skimage.morphology.remove_small_objects(mask, min_size=area/8)

    smallstuff = np.logical_and(mask, np.logical_not(bigstuff))
    smallstuff = skimage.morphology.binary_dilation(smallstuff, selem=skimage.morphology.disk(5))

    mask = np.logical_not(smallstuff)

    return mask

def show_pair(before, after):
    diff = after-before

    plt.figure( figsize=(20,20))
    plt.imshow(np.concatenate((before,diff), axis=1))
    plt.axis('off')

    #fig, ax = skimage.filters.try_all_threshold(image)
    plt.show()

def reshape(image):
    h, w = image.shape
    image = image.astype(float)

    image -= floor(image)
    image  = np.maximum(image, 0)
    image /= np.max(image)

    # Flip image if it points to the left
    weight = np.mean(image, axis=0)
    #print(weight)

    if sum(weight[0:w//2]) < sum(weight[w//2:w]):
        image  = np.fliplr(image)

    # Trim white frame around (if any)
    wmap = np.median(image[h*20//100:h*80//100,:], axis=0)
    assert(len(wmap)==w)

    l = 0
    while l < w*20//100 and np.median(wmap[l:l+4]) > .6:
        l += 1

    r = w
    while r > w*80//100 and np.median(wmap[r-4:r]) > .2:
        r -= 1

    hmap = np.median(image[:,r*70//100:r], axis=1)
    assert(len(hmap)==h)

    t = 0
    while t < h*20//100 and np.median(hmap[t:t+4]) > .2:
        t += 1

    if t != 0:
        t += h//200

    b = h
    while b > h*80//100 and np.median(hmap[b-4:b]) > .2:
        b -= 1

    if b != h:
        b -= h//100

    # Trim exessive black from the right and from the bottom
    wmap = np.median(image[(t+b)//2:b,:], axis=0)
    assert(len(wmap)==w)

    while np.mean(wmap[r-(r-l)//5:r]) < .02:
        r -= 1

    hmap = np.median(image[:,l+(r-l)//10:r], axis=1)
    assert(len(hmap)==h)

    while np.mean(hmap[b*9//10:b]) < .02:
        b -= 1

    # Actually crop
    image = image[t:b, l:r]
    w = r-l
    h = b-t

    if w > h//2:
        # Fit width of the fatty image
        w, h = 1024, h*1024//w
    else:
        # Fit height of a skinny image
        w, h = w*2048//h, 2048

    image = cv2.resize(image, (w,h), cv2.INTER_AREA)
    #print(image.shape)

    image *= gen_mask(image)

    image -= floor(image)
    image  = np.maximum(image, 0)
    image /= np.max(image)

    if h < 2048:
        # Pad fatty image equally on top and bottom
        image = np.pad(image, (((2048-h)//2, (2048+1-h)//2),(0,0)), 'constant')
    elif w < 1024:
        # Pad skinny image on the right
        image = np.pad(image, ((0,0), (0, 1024-w)), 'constant')

    assert(image.shape == (2048, 1024))

    return image

def convert_images(src_folder, dst_folder, ext, csv_name=None):

    image_files = sorted(os.listdir(src_folder))

    if not os.path.exists(dst_folder):
        print("Creating " + dst_folder)
        os.makedirs(dst_folder)

    csvfile = open(os.path.join(data_folder, csv_name), 'w', newline='') if csv_name else None
    writer = None
    if not csvfile is None:
        cols = ['File', 'Rows', 'Cols', 'Bits', 'Min', 'Max']
        writer = csv.DictWriter(csvfile, fieldnames=cols)
        writer.writeheader()

    def load_and_log(name):
        #print(name)
        image_path = os.path.join(src_folder, name)
        image = cv2.imread(image_path, cv2.IMREAD_ANYDEPTH)
        if image is None:
            return image

        if not writer is None:
            row = {
                'File': name,
                'Rows': image.shape[0],
                'Cols': image.shape[1],
                'Bits': image.dtype,
                'Min': np.min(image),
                'Max': np.max(image),
            }

            writer.writerow(row)

        return image

    paired = None
    progress = tqdm(image_files)
    for src_name in progress:
        if not ext in src_name:
            continue

        if src_name == paired:
            continue

        progress.set_description(src_name)

        CC  = None
        MLO = None

        if "CC" in src_name:
            CC  = load_and_log(src_name)
            paired = src_name.replace('CC', 'MLO')
            MLO = load_and_log(paired)
        else:
            CC  = None
            paired = None
            MLO  = load_and_log(src_name)

        if not CC is None and not MLO is None:
            CC  = reshape(CC)
            MLO = reshape(MLO)

            pair = np.concatenate((CC, MLO), axis=1)
            assert(pair.shape ==(2048, 2048))

            pair = (pair*65535).astype(np.uint16)

            assert('_CC' in src_name)
            png_name = src_name.replace('_CC', '').replace(ext,'png')

            cv2.imwrite(os.path.join(dst_folder, png_name), pair, [cv2.IMWRITE_PNG_COMPRESSION, 9])

def tiff_to_png(data_folder, csv_name=None):
    src_folder = os.path.join(data_folder, 'tif')
    dst_folder = os.path.join(data_folder, 'png')

    convert_images(srt_folder, dst_folder, 'tif', csv_name)


#tiff_to_png('../data/ddsm')#, 'ddsm.csv')

def case_to_images(case_folder, dst_folder, out_format='png'):
    files = os.listdir(case_folder)

    def get_file(token):
        for file in files:
            if '.1' in file or '.2' in file:
                continue

            if token in file:
                return file

        return None

def parse_ics(ics_path):
    def parse_mode(tokens):
        result = {}
        for key, value in zip(tokens[0::2], tokens[1::2]):
            if key == 'LINES':
                result['height'] = int(value)
            elif key == 'PIXELS_PER_LINE':
                result['width'] = int(value)
            elif key == 'BITS_PER_PIXEL':
                result['precision'] = int(value)

        return result


    def parse_tokens(tokens):
        head, *tail = tokens

        if head == 'DIGITIZER':
            return {'digitizer': ' '.join(tail).strip()}

        if head in ('LEFT_CC','LEFT_MLO','RIGHT_CC','RIGHT_MLO'):
            return { head: parse_mode(tail)}

        return {}

    ics = {}
    for line in open(ics_path):
        ics.update(parse_tokens(line.split(' ')))

    return ics

def case_to_images(case_folder, dst_folder, out_format='png'):
    files = os.listdir(case_folder)

    def get_file(token):
        for file in files:
            if '.1' in file:
                continue

            if token in file:
                return file

        return None

    ics = parse_ics(os.path.join(case_folder, get_file('.ics')))

    gamma = {
        'DBA 21':([0,185,271,393,581,847,1217,1772,2603,3797,5648,8296,12569,18445,26986,37201,55722,65535],
                  [0,22391,25304,27852,31129,33860,36590,39321,42234,44964,48059,50608,52974,56433,59346,61712,64807,65535]),
    }

    def raw_to_image(mode):
        header = ics[mode]
        if header is None:
            print("Warning! '"+mode+"' is not listed in "+case_folder+"/*.ics")
            return

        ljpeg_name = get_file(mode + '.LJPEG')
        if ljpeg_name is None:
            print("Warning! '"+ljpeg_name+"' is not found in "+case_folder)
            return

        precision = header['precision']
        dtype = np.uint16 if precision > 8 else np.uint8
        size = (header['height'],header['width'])

        ljpeg_path = os.path.join(case_folder, ljpeg_name)
        img_path = os.path.join(dst_folder, ljpeg_name.replace('LJPEG', out_format))

        existing = cv2.imread(img_path, cv2.IMREAD_ANYDEPTH)
        if not( existing is None ) and existing.shape == size and existing.dtype == dtype:
            return

        os.system('/home/ksebov/_dev/JPEGv1.2.1_LINUXport/jpegdir/jpeg -d -s "' + ljpeg_path +'" >/dev/null 2>&1')

        raw_path = ljpeg_path + '.1'

        buffer = np.fromfile(raw_path, dtype=dtype)
        if buffer is None:
            return

        assert(len(buffer)==size[0]*size[1])
        buffer.byteswap(inplace=True)

        digitizer = ics['digitizer']
        if digitizer in gamma:
            xp, fp = gamma[digitizer]
            buffer = np.interp(buffer, xp, fp).astype(np.uint16)
        elif precision in range(8, 16):
            buffer *= 2**(16-precision)

        assert(len(buffer)==size[0]*size[1])

        #print(np.min(buffer), np.max(buffer))

        cv2.imwrite(img_path, buffer.reshape(size), [cv2.IMWRITE_PNG_COMPRESSION, 9])

        os.remove(raw_path)

    raw_to_image('LEFT_CC')
    raw_to_image('LEFT_MLO')
    raw_to_image('RIGHT_CC')
    raw_to_image('RIGHT_MLO')


def cases_to_images(group_folder, dst_folder, out_format='png'):
    cases = sorted(os.listdir(group_folder))

    progress = tqdm(cases)
    for case in progress:
        case_folder = os.path.join(group_folder, case)
        if not os.path.isdir(case_folder):
            continue

        progress.set_description(case)
        case_to_images(case_folder, dst_folder, out_format)

def summarize_ics(dir, csv_name='summary.csv'):
    files = sorted(os.listdir(dir))

    cols = ['File', 'Rows', 'Cols', 'Bits']
    csvfile = open(os.path.join(dir, csv_name), 'w', newline='')

    writer = csv.DictWriter(csvfile, fieldnames=cols)

    writer.writeheader()

    progress = tqdm(files)
    for file in progress:
        if not '.ics' in file:
            continue

        progress.set_description(file)

        ics = parse_ics(os.path.join(dir, file))

        def log_mode(mode):
            header = ics[mode]
            if header is None:
                print("Warning! '"+mode+"' not found in"+file)

                return

            row = {
                'File': file,
                'Rows': header['height'],
                'Cols': header['width'],
                'Bits': header['precision'],
            }

            writer.writerow(row)

        log_mode("LEFT_CC")
        log_mode("LEFT_MLO")
        log_mode("RIGHT_CC")
        log_mode("RIGHT_MLO")

#summarize_ics('../data/ddsm/ics')
import matplotlib.pyplot as plt
#from skimage.segmentation import chan_vese
from skimage.morphology import watershed, disk
from skimage import data
from skimage.filters import median, sobel, sobel_h, sobel_v


def show3(*images):
    fig, ax = plt.subplots(1,3, figsize=(18, 8))

    ax[0].imshow(images[0])
    ax[0].axis('off')

    ax[1].imshow(images[1])
    ax[1].axis('off')

    ax[2].imshow(images[2])
    ax[2].axis('off')

    plt.show()

def show_file_mask(image_path):
    image = cv2.imread(image_path, cv2.IMREAD_ANYDEPTH)
    if image is None:
        print("Warning! Couldn't load "+image_path)

        return

    #image = cv2.resize(image, (512,1024), cv2.INTER_AREA)
    image = image.astype(np.float32)/65535

    print(np.min(image), np.max(image))

    #image = image[:,0:1024]
    #mask = gen_mask(image)
    #masked = image*mask
    #show3(image, mask, image-masked)

    denoised = image#median(image, disk(2))
    gradient = sobel(denoised)

    show3(image, abs(sobel_h(image)), abs(sobel_v(image)))

def show_dir_mask(dir):
    files = sorted(os.listdir(dir))

    progress = tqdm(files)
    for file in progress:
        if 'mask' in file:
            continue

        progress.set_description(file)

        show_file_mask(os.path.join(dir,file))

#show_file_mask('../data/test/A_1177_1.LEFT.png')

#show_dir_mask('F:\\ddsm\\png\\benigns')

