import pickle
import numpy as np
import tensorflow as tf
import PIL.Image
import dataset_tool
import cv2
import math
from tqdm import tqdm

def generate_random_images():
    # Initialize TensorFlow session.
    tf.InteractiveSession()

    # Import official CelebA-HQ networks.
    with open('../results/006-pgan-DDSM-labeled-preset-v2-8gpus-fp32/network-final.pkl', 'rb') as file:
        G, D, Gs = pickle.load(file)

    # Generate latent vectors.
    latents = np.random.RandomState(1000).randn(1000, *Gs.input_shapes[0][1:]) # 1000 random latents
    latents = latents[[477, 56, 83, 887, 583, 391, 86, 340, 341, 415]] # hand-picked top-10

    # Generate dummy labels (not used by the official networks).
    labels = np.zeros([latents.shape[0]] + Gs.input_shapes[1][1:])

    # Run the generator to produce a set of images.
    images = Gs.run(latents, labels)

    # Convert images to PIL-compatible format.
    images = np.clip(np.rint((images + 1.0) / 2.0 * 255.0), 0.0, 255.0).astype(np.uint8) # [-1,1] => [0,255]
    #images = images.transpose(0, 2, 3, 1) # NCHW => NHWC
    images = np.reshape(images, (-1, images.shape[2], images.shape[3]))

    # Save images as PNG.
    for idx in range(images.shape[0]):
        PIL.Image.fromarray(images[idx], 'L').save('img%d.png' % idx)

def confusion_matrix(gtruth, result, mask, matrix = None):
    assert(gtruth.shape[0]<=result.shape[0] and gtruth.shape[1:]==result.shape[1:])
    num_samples, num_classes = gtruth.shape

    if matrix is None:
        matrix = np.zeros((num_classes, num_classes))

    for x, y, test in zip(gtruth, result, mask):
        if not test:
            continue

        if np.amax(x) < .5:
            continue # Ignore unlabeled

        matrix[np.argmax(x), np.argmax(y)] += 1

    return matrix

def pntf_stats(m, gtruth, result, mask, stats = None): # [Pos=0/Neg=1, True=0/False=1]
    assert(gtruth.shape[0]<=result.shape[0] and gtruth.shape[1:]==result.shape[1:])
    num_samples, num_classes = gtruth.shape

    if stats is None:
        stats = np.zeros((2,2))

    for x, y, test in zip(gtruth, result, mask):
        if not test:
            continue

        if np.amax(x) < .5:
            continue # Ignore unlabeled

        true_m = np.argmax(x)
        pred_m = np.argmax(y)

        stats[ 0 if pred_m == m else 1, 0 if true_m == pred_m else 1 ] += 1

    return stats


def is_list(data):
    return type(data) is list

def partition(data, start, end):
    return [d[start:end] for d in data] if is_list(data) else data[start:end]

def get_minibatches(data, minibatch_size):
    total = len(data[0]) if is_list(data) else len(data)

    for minibatch_start in np.arange(0, total, minibatch_size):
        yield partition(data, minibatch_start, min(total, minibatch_start + minibatch_size))

def infer_ddsm(image_dir, snapshot, stats_class, filter = None):
    image_filenames, gtruth, rand = dataset_tool.process_ddsm_dir(image_dir)
    results = np.zeros(gtruth.shape)

    total_samples = len(image_filenames)
    num_classes = gtruth.shape[1]

    batch_size = 8
    image_batch = np.zeros((batch_size, ), dtype=np.float32)

    img = cv2.imread(image_filenames[0], cv2.IMREAD_ANYDEPTH)
    image_batch = np.zeros([batch_size, 1, 1024, 1024])

    conf_matrix = np.zeros((num_classes, num_classes))
    stats       = np.zeros((2, 2))

    assert(len(gtruth.shape)==2)
    label_batch = np.ones((batch_size, num_classes), dtype=np.float32)/num_classes

    # Initialize TensorFlow session.
    tf.InteractiveSession()

    with open(snapshot, 'rb') as file:
        G, D, Gs = pickle.load(file)

    progress = tqdm(get_minibatches([results, image_filenames, gtruth, rand], batch_size), total=total_samples//batch_size)
    for results_batch, names_batch, gtruth_batch, rand_batch in progress:
        mask = [True]*batch_size if filter is None else filter(rand_batch)

        for sample, image_name, test in zip(image_batch, names_batch, mask):
            if not test:
                continue

            img = cv2.imread(image_name, cv2.IMREAD_ANYDEPTH).astype(np.float32)

            img = (img[0::2, 0::2] + img[0::2, 1::2] + img[1::2, 0::2] + img[1::2, 1::2]) * 0.25
            assert(img.shape==(1024,1024))

            img = (img-(2**15))/2**15

            sample[0] = img

        """
        image_batch[0] = np.zeros_like(image_batch[0])
        image_batch[1] = np.zeros_like(image_batch[0])
        image_batch[2] = np.ones_like(image_batch[0])
        image_batch[3] = np.ones_like(image_batch[0])
        image_batch[4] = np.ones_like(image_batch[0])*-1
        image_batch[5] = np.ones_like(image_batch[0])*-1
        """
        """
        images = np.clip(np.rint((image_batch + 1.0) / 2.0 * 255.0), 0.0, 255.0).astype(np.uint8) # [-1,1] => [0,255]
        images = np.reshape(images, (-1, images.shape[2], images.shape[3]))

        # Save images as PNG.
        for idx in range(images.shape[0]):
            PIL.Image.fromarray(images[idx], 'L').save('img%d.png' % idx)
        """

        scores, labels = D.run(image_batch)
        results_batch = labels

        conf_matrix = confusion_matrix       (gtruth_batch, results_batch, mask, conf_matrix)
        stats       = pntf_stats(stats_class, gtruth_batch, results_batch, mask, stats)

        tp, fp, tn, fn = stats.flatten()

        epsilon = 1e-6
        accuracy = (tp+tn)/np.sum(stats)
        precision = tp/(tp+fp+epsilon)
        recall    = tp/(tp+fn+epsilon)

        f1 = 2/(1/(recall+epsilon) + 1/(precision+epsilon))

        #print(scores)
        #print(labels)
        print(conf_matrix)
        print("Accuracy: %4.2f%%, F1: %4.2f%%" % (accuracy*100, f1*100))

    return image_filenames, gtruth, rand, results

"""
infer_ddsm('../data/ddsm-709/pairs',
           '../results/008-pgan-ddsm-709-labeled-cond-preset-v2-8gpus-fp32/network-final.pkl',
           1, # Cancer
           lambda x : x > 0.8) # Test set

infer_ddsm('../data/ddsm-709/pairs',
           '../results/008-pgan-ddsm-709-labeled-cond-preset-v2-8gpus-fp32/network-final.pkl',
           1, # Cancer
           lambda x : x <= 0.8) # Training set

infer_ddsm('../data/DDSM/pairs',
           '../results/007-pgan-DDSM-labeled-cond-preset-v2-8gpus-fp32/network-final.pkl',
           3, # Cancer
           lambda x : x <= 0.8) # Training set

infer_ddsm('../data/DDSM/pairs',
           '../results/007-pgan-DDSM-labeled-cond-preset-v2-8gpus-fp32/network-final.pkl',
           3, # Cancer
           lambda x : x > 0.8) # Test set
"""
